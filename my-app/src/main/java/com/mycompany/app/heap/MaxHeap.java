package com.mycompany.app.heap;

public class MaxHeap implements Heap {

    public int[] heap;
    public int size = 0;

    public MaxHeap(int capacity) {
        this.heap = new int[capacity];
    }

    public void insert(int value) {
        if(isFull()) {
            throw new IndexOutOfBoundsException("Heap is full");
        }
        heap[size] = value;
        heapifyUp(size);
        size++;
    }

    public boolean isFull() {
        return size == heap.length;
    }

    public void heapify(int i) {
        int l = Heap.getLeft(i);
        int r = Heap.getRight(i);
        int largest = i;
        if (l <= this.size && heap[l] > heap[i]) {
            largest = l;
        }
        if (r <= this.size && heap[r] > heap[largest]) {
            largest = r;
        }
        if (largest != i) {
            int temp = heap[i];
            heap[i] = heap[largest];
            heap[largest] = temp;
            heapify(largest);
        }
    }

    @Override
    public void heapifyUp(int i) {
        int newValue = heap[i];
        int pi = Heap.getParent(i);
        while (i > 0 && heap[i] > heap[pi]) {
            heap[i] = heap[pi];
            i = pi;
        }
        heap[i] = newValue;
    }

}
