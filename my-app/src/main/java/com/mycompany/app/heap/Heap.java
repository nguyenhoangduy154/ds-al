package com.mycompany.app.heap;

public interface Heap {

    boolean isFull();

    void insert(int value);

    void heapify(int i);

    void heapifyUp(int i);
    public static int getParent(int i) {
        return (i - 1) / 2;
    }

    public static int getLeft(int i) {
        return 2 * i + 1;
    }

    public static int getRight(int i) {
        return  2 * i + 2;
    }
}
