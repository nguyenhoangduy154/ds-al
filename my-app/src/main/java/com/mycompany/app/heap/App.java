package com.mycompany.app.heap;

import com.mycompany.app.heap.Heap;
import com.mycompany.app.heap.MaxHeap;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        PriorityQueue<Integer>  pq = new PriorityQueue<>(); // using min-heap
        pq.add(5);
        pq.add(2);
        pq.add(25);
        pq.add(10);
        pq.add(7);
        pq.add(20);
        for (Object o : pq.toArray()) {
            System.out.println(o);
        }
//        System.out.println(pq.peek());
//        System.out.println(pq.remove(50));
//        System.out.println(pq.peek());
        System.out.println(pq.poll());
//        System.out.println(pq.peek());

//        MaxHeap maxHeap = new MaxHeap(10);
//        maxHeap.insert(4);
//        maxHeap.insert(14);
//        maxHeap.insert(7);
//        maxHeap.insert(2);
//        maxHeap.insert(8);
//        maxHeap.insert(1);
////        maxHeap.heapify(0);
//        System.out.println(Arrays.toString(maxHeap.heap));
    }
}
