package com.mycompany.app.heap;

import java.util.Arrays;

public class MyPriorityQueue {
    /**
     * This simple priority queue use max-heap property.
     * index = 0 mean the most priority.
     * Some function in class:
     * peek() -> get the root value
     * poll() -> remove return the most priority.
     * offer(x) -> insert x to queue.
     * heapifyUp(int i) -> main tain heap above
     * heapifyDown(int i) -> main tain heap below
     *            0
     *      1           2
     *  3       4   5       6
     *
     */
    private int[] queue;
    private int size;

    public MyPriorityQueue(int capacity) {
        queue = new int[capacity];
    }


    /**
     * get parent node
     * @param i
     * @return parent node
     */
    private int getParent(int i) {
        return (i - 1) >>> 1;
    }

    /**
     * get left child node
     * @param i
     * @return index of left child node
     */
    private int getLeft(int i) {
        return (i << 1) + 1;
    }

    /**
     * get right child node
     * @param i
     * @return index of right child node
     */
    private int getRight(int i) {
        return 2 * (i + 1);
    }

    /**
     * maintain heap child node.
     * @param i
     */
    public void heapifyDown( int i) {
        int largest = i;
        while (largest < size) {
            int l = getLeft(i);
            int r = getRight(i);
            if(l < size && queue[l] > queue[i]) {
                largest = l;
            }
            if(r < size && queue[r] > queue[largest]) {
                largest = r;
            }
            if (largest != i) {
                int temp = queue[i];
                queue[i] = queue[largest];
                queue[largest] = temp;
                i = largest;
            } else {
                break;
            }
        }

    }

    public void heapifyUp(int i) {
        int key = queue[i];
        while (i > 0) {
            int p = getParent(i);
            if (queue[p] < key) {
                queue[i] = queue[p];
                i = p;
            } else
                break;
        }
        queue[i] = key;
    }

    /**
     * Insert new value and fix the max-heap
     * usually
     * @param x
     */
    public void offer(int x) {
        queue[size++] = x;
        heapifyUp(size - 1);
    }

    public int peek() {
        return queue[0];
    }

    /**
     * remove and return the largest value
     * @return
     */
    public int poll() {
        int max = queue[0];
        queue[0] = queue[--size];
        // not necessary to remove
        queue[size] = 0;
        heapifyDown(0);
        return  max;
    }
}
