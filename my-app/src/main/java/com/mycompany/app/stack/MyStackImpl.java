package com.mycompany.app.stack;

public class MyStackImpl<T> implements MyStack<T> {

    public int MAX_LENGTH = 5;
    public final Object[] s;
    private int pointer = -1;

    public MyStackImpl() {
        s = new Object[MAX_LENGTH];
    }

    @Override
    public void push(T val) {
       if (isFull()) {
           System.out.println("Stack is full, cannot add new item");
       } else {
           s[++pointer] = val;
       }
    }

    @Override
    public T pop() {
        T val = null;
        if (isEmpty()) {
            System.out.println("Stack is empty.");
        } else {
             val = (T)s[pointer--];
        }
        return val;
    }

    @Override
    public boolean isEmpty() {
        if (pointer == -1)
            return true;
        return false;
    }

    @Override
    public boolean isFull() {
        if (pointer + 1 == MAX_LENGTH)
            return true;
        return false;
    }
}
