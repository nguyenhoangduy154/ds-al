package com.mycompany.app.stack;

public interface MyStack<T> {

    /**
     * Insert item into stack
     */
    void push(T val);

    /**
     * remove the last inserted item.
     * @return return last inserted item
     */
    T pop();

    /**
     * Check whether stack is empty
     * @return true if stack is empty
     */
    boolean isEmpty();

    /**
     * check whether stack is full
     * @return
     */
    boolean isFull();

}
