package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Color;

import java.util.Stack;


public class Dfs implements Traversal {

    protected int count = 0;

    @Override
    public void travel(Vertex[] G, String vertex) {
        for (Vertex u: G) {
            if (u.color == Color.WHITE) {
                visitDfsNoRecur(u);
            }
        }
    }

    // DFS no recursive
    // Recursive likes a stack. So, just use stack for eliminating the Recursive.
    protected void visitDfsNoRecur(Vertex u) {
        boolean isWhite = false;
        Stack<Vertex> stack = new Stack<>();
        u.dTime = ++count;
        u.color = Color.GRAY;
        stack.push(u);
        while (!stack.isEmpty()) {
            u = stack.peek(); // process current vertex
            for (Vertex v: u.descendant) { // find the descendant vertex
                if (v.color == Color.WHITE) {
                    System.out.println("("+ u.name + ", " + v.name +") - " + Edge.TREE);
                    isWhite = true;
                    v.dTime = ++count;
                    v.color = Color.GRAY;
                    v.ancestor = u;
                    stack.push(v);
                    break;
                } else if (v.color == Color.GRAY)
                    System.out.println("("+ u.name + ", " + v.name +") - " + Edge.BACK);
                else if (v.dTime > u.dTime)
                    System.out.println("("+ u.name + ", " + v.name +") - " + Edge.FORWARD);
                else
                    System.out.println("("+ u.name + ", " + v.name +") - " + Edge.CROSS);

            }
            if (isWhite) {
                isWhite = false;
                continue;
            }
            u.color = Color.BLACK;
            u.fTime = ++count;
            stack.pop();
        }

    }
    //DFS Visit
    private void visitDFS(Vertex u) {
        count++;
        u.dTime = count;
        u.color = Color.GRAY;
        for (Vertex v : u.descendant) {
            if (v.color == Color.WHITE) {
                v.ancestor = u;
                visitDFS(v);
            }
        }
        u.color = Color.BLACK;
        count++;
        u.fTime = count;
    }

}
