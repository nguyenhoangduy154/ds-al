package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Color;

import java.util.LinkedList;
import java.util.Queue;

public class Bfs implements Traversal {

    @Override
    public void travel(Vertex[] G, String vertex) {
        Queue<Vertex> q = new LinkedList();
        // find vertex
        Vertex src = null;
        for (Vertex v : G) {
            if (v.name.equals(vertex)) {
                src = v;
                break;
            }

        }
        if (src == null)
            return;

        src.color = Color.GRAY;
        src.weight = 0;
        src.ancestor = null;
        q.offer(src);
        while (!q.isEmpty()) {
            Vertex curV = q.poll();
            for (Vertex v : curV.descendant) {
                if (v.color == Color.WHITE) {
                    v.color = Color.GRAY;
                    v.weight = curV.weight + 1;
                    v.ancestor = curV;
                    q.offer(v);
                }
            }
            curV.color = Color.BLACK;
        }
    }
}
