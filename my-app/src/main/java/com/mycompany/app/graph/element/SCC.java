package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Graph;

public interface SCC {
    void foundScc(Vertex[] G);
}
