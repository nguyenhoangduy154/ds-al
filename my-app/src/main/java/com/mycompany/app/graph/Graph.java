package com.mycompany.app.graph;


import com.mycompany.app.graph.element.Dfs;
import com.mycompany.app.graph.element.TopologicalSort;
import com.mycompany.app.graph.element.Traversal;
import com.mycompany.app.graph.element.Vertex;
import com.mycompany.app.graph.mock.MockGraph;

import java.util.List;

public class Graph {

    //Traversal
    Traversal dfs = new Dfs();

    public void travel(Vertex[] G, String vertex) {
        dfs.travel(G, vertex);
    }

    public List<Vertex> sort(Vertex[] G) {
        TopologicalSort topo = new TopologicalSort();
        topo.travel(G, "");
        return topo.sortList;
    }

    public int countPath(Vertex[] G, String from, String to) {
        TopologicalSort topo = new TopologicalSort();
        topo.travel(G, "");
        return topo.countPath(from, to);
    }

    public Vertex[] transpose(Vertex[] G) {
        int noVertex = G.length;
        Vertex[] Gt = new Vertex[noVertex];

        // Copy vertex
        for (int i = 0; i < noVertex; i++) {
            Gt[i] = new Vertex(G[i].name);
        }
        // Create transpose edges
        for (int i = 0; i < noVertex; i++) {
            for (int j = 0; j < G[i].descendant.size(); j++) {
                int id = getIndex(Gt, G[i].descendant.get(j).name);
                Gt[id].descendant.add(Gt[i]);
            }
        }
        return Gt;
    }

    public int getIndex(Vertex[] Gt, String s) {
        for (int i = 0; i < Gt.length; i++) {
            if (Gt[i].name.equals(s))
                return i;
        }
        return -1;
    }
}
