package com.mycompany.app.graph.mock;

import com.mycompany.app.graph.element.Vertex;

import java.util.ArrayList;

public class GraphTest1 implements MockGraph{

    /*
        Undirected Graph from figure 22.1
     */

    @Override
    public Vertex[] createAdjacencyList() {
        Vertex[] G = new Vertex[5];

        //Create vertex
        G[0] = new Vertex("1");
        G[1] = new Vertex("2");
        G[2] = new Vertex("3");
        G[3] = new Vertex("4");
        G[4] = new Vertex("5");

        // Create edge
        // vx 1
        G[0].descendant.add(G[1]);
        G[0].descendant.add(G[4]);

        // vx 2
        G[1].descendant.add(G[0]);
        G[1].descendant.add(G[1]);
        G[1].descendant.add(G[2]);
        G[1].descendant.add(G[3]);
        G[1].descendant.add(G[4]);

        // vx 3
        G[2].descendant.add(G[1]);
        G[2].descendant.add(G[3]);

        // vx 4
        G[3].descendant.add(G[1]);
        G[3].descendant.add(G[2]);
        G[3].descendant.add(G[4]);

        // vx 5
        G[4].descendant.add(G[0]);
        G[4].descendant.add(G[1]);
        G[4].descendant.add(G[3]);

        return G;
    }
}
