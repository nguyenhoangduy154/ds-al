package com.mycompany.app.graph.mock;

import com.mycompany.app.graph.element.Vertex;

public class GraphTest3 implements MockGraph{

    /*
        Directed graph from figure 22.8
     */

    @Override
    public Vertex[] createAdjacencyList() {
        Vertex[] G = new Vertex[14];
        G[0] = new Vertex("m");
        G[1] = new Vertex("n");
        G[2] = new Vertex("o");
        G[3] = new Vertex("p");
        G[4] = new Vertex("q");
        G[5] = new Vertex("r");
        G[6] = new Vertex("s");
        G[7] = new Vertex("t");
        G[8] = new Vertex("u");
        G[9] = new Vertex("v");
        G[10] = new Vertex("w");
        G[11] = new Vertex("x");
        G[12] = new Vertex("y");
        G[13] = new Vertex("z");

        // Create edge
        // Vertex m
        G[0].descendant.add(G[4]);
        G[0].descendant.add(G[5]);
        G[0].descendant.add(G[11]);
        // Vertex n
        G[1].descendant.add(G[4]);
        G[1].descendant.add(G[8]);
        G[1].descendant.add(G[2]);
        // Vertex o
        G[2].descendant.add(G[5]);
        G[2].descendant.add(G[6]);
        G[2].descendant.add(G[9]);
        // Vertex p
        G[3].descendant.add(G[2]);
        G[3].descendant.add(G[6]);
        G[3].descendant.add(G[13]);
        // Vertex q
        G[4].descendant.add(G[7]);
        // Vertex r
        G[5].descendant.add(G[8]);
        G[5].descendant.add(G[12]);
        // Vertex s
        G[6].descendant.add(G[5]);
        // Vertex t
        // Vertex u
        G[8].descendant.add(G[7]);
        // Vertex v
        G[9].descendant.add(G[10]);
        G[9].descendant.add(G[11]);
        // Vertex w
        G[10].descendant.add(G[13]);
        // Vertex x
        // Vertex y
        G[12].descendant.add(G[9]);
        // Vertex z

        return G;
    }
}
