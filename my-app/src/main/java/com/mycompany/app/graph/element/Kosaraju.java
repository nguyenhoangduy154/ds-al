package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Color;
import com.mycompany.app.graph.Graph;
import com.mycompany.app.graph.mock.GraphTest3;

import java.util.List;

public class Kosaraju implements SCC{

    Graph graph = new Graph();

    @Override
    public void foundScc(Vertex[] G) {
        // step 1: compute dfs --> actually we use topo sort here.
        // return stack (pseudo linked list)
        // just get name
        List<Vertex> stack = graph.sort(G);

        // step 2: compute G transpose
        Vertex[] Gt = graph.transpose(G);

        printConnectedComponent(Gt, stack);
    }

    private void printConnectedComponent(Vertex[] Gt, List<Vertex> src) {
        int root =  0;
        while (root < src.size()) {
            int idx = graph.getIndex(Gt, src.get(root).name);
            root = root + dfsAndPrint(Gt, Gt[idx]);
            System.out.println("");
        }
    }

    private int dfsAndPrint(Vertex[] Gt, Vertex u) {
        int count = 1;
        u.color = Color.GRAY;
        for (Vertex v: u.descendant) {
            if (v.color == Color.WHITE) {
                count += dfsAndPrint(Gt, v);
            }
        }
        u.color = Color.BLACK;
        System.out.print(u.name + ", ");
        return count;
    }
}
