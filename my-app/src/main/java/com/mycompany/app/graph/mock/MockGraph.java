package com.mycompany.app.graph.mock;

import com.mycompany.app.graph.element.Vertex;

public interface MockGraph {
   /*
      Create specific graph
    */
   Vertex[] createAdjacencyList();
   default Vertex[][] createAdjacencyMatrix() {return new Vertex[1][1];};
}
