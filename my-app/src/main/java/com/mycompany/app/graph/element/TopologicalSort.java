package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Color;

import java.util.*;

public class TopologicalSort extends Dfs {

    public LinkedList<Vertex> sortList = new LinkedList<>();

    public int countPath(String fromVertexName, String toVertexName) {
        // Stack
        Stack<Vertex> stack1 = new Stack<>();
        // return
        int count = 0;
        // List vertex from - to Vertex
        List<Vertex> extractList = extractTopologicalList(fromVertexName, toVertexName);

        // Number of vertex in list
        int noOfVertex = extractList.size();
        //
        Vertex fromVertex = extractList.get(0);
        Vertex toVertex = extractList.get(noOfVertex - 1);
        //
        // Processing
//        return countPathRecur(extractList, toVertex, 0);
        return countPathBottomUp(extractList, noOfVertex);
    }

    public int countPathBottomUp(List<Vertex> lst, int n) {
        int[] memo = new int[n];
        Arrays.fill(memo, 0);
        memo[n - 1] = 1;
        for (int i = n - 2; i >= 0; i--) {
            for (Vertex v : lst.get(i).descendant) {
                for (int j = i + 1; j < n; j++) {
                    if (v == lst.get(j)) {
                        memo[i] += memo[j];
                        break;
                    }
                }
            }
        }
        return  memo[0];
    }

    //Count path using DP top-down
    public int countPathDp(int[] memo, List<Vertex> lst, int idx) {
        int count = 0;
        if (memo[idx] != -1) {
            return memo[idx];
        }

        for (Vertex v : lst.get(idx).descendant) {
            for (int i = 0; i < lst.size(); i++) {
                if (v == lst.get(i)) {
                    count += countPathDp(memo, lst, i);
                    break;
                }
            }
        }
        memo[idx] = count;
        return count;
    }

    //Count path Recur
    public int countPathRecur(List<Vertex> lst, Vertex destination, int idx) {
        int count = 0;
        if (lst.get(idx) == destination) {
            return 1;
        }

        for (Vertex v : lst.get(idx).descendant) {
            for (int i = 0; i < lst.size(); i++) {
                if (v == lst.get(i)) {
                    count += countPathRecur(lst, destination, i);
                    break;
                }
            }
        }
        return count;
    }

    // Return Array of Vertex from vertex u to vertex v
    public List<Vertex> extractTopologicalList(String fromVertex, String toVertex) {
        List<Vertex> lst = new ArrayList<>();
        boolean isMet = false;
        for (Vertex v: sortList) {
            if (v.name.equals(fromVertex)) {
                lst.add(v);
                isMet = true;
                continue;
            }
            if (v.name.equals(toVertex)) {
                lst.add(v);
                break;
            }
            if (isMet) {
                lst.add(v);
            }
        }
        return lst;
    }

    @Override
    protected void visitDfsNoRecur(Vertex u) {
        boolean isWhite = false;
        Stack<Vertex> stack = new Stack<>();
        u.dTime = ++count;
        u.color = Color.GRAY;
        stack.push(u);
        while (!stack.isEmpty()) {
            u = stack.peek(); // process current vertex
            for (Vertex v: u.descendant) { // find the descendant vertex
                if (v.color == Color.WHITE) {
                    isWhite = true;
                    v.dTime = ++count;
                    v.color = Color.GRAY;
                    v.ancestor = u;
                    stack.push(v);
                    break;
                }
            }
            if (isWhite) {
                isWhite = false;
                continue;
            }
            u.color = Color.BLACK;
            u.fTime = ++count;
            sortList.addFirst(u);
            stack.pop();
        }
    }
}
