package com.mycompany.app.graph.mock;

import com.mycompany.app.graph.element.Vertex;

public class GraphTest2 implements MockGraph{

    /*
        Graph from figure 22.6
     */
    @Override
    public Vertex[] createAdjacencyList() {
        Vertex[] G = new Vertex[10];
        // Create Vertex
        G[0] = new Vertex("q");
        G[1] = new Vertex("s");
        G[2] = new Vertex("v");
        G[3] = new Vertex("w");
        G[4] = new Vertex("t");
        G[5] = new Vertex("x");
        G[6] = new Vertex("z");
        G[7] = new Vertex("y");
        G[8] = new Vertex("r");
        G[9] = new Vertex("u");

        // Create edge

        // vertex q
        G[0].descendant.add(G[1]);
        G[0].descendant.add(G[3]);
        G[0].descendant.add(G[4]);
        // vertex s
        G[1].descendant.add(G[2]);
        // vertex v
        G[2].descendant.add(G[3]);
        // vertex w
        G[3].descendant.add(G[1]);
        // vertex t
        G[4].descendant.add(G[5]);
        G[4].descendant.add(G[7]);
        // vertex x
        G[5].descendant.add(G[6]);
        // vertex z
        G[6].descendant.add(G[5]);
        // vertex y
        G[7].descendant.add(G[0]);
        // vertex r
        G[8].descendant.add(G[7]);
        G[8].descendant.add(G[9]);
        // vertex u
        G[9].descendant.add(G[7]);

        return G;
    }
}
