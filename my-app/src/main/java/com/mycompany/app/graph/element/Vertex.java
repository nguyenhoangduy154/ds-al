package com.mycompany.app.graph.element;

import com.mycompany.app.graph.Color;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

    public int dTime;
    public int fTime;
    public String name;
    public int weight = 1;
    public Color color;
    public List<Vertex> descendant;
    public Vertex ancestor;

    public Vertex(String name) {
        this.color = Color.WHITE;
        this.name = name;
        descendant = new ArrayList<>();
    }
}
