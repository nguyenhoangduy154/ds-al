package com.mycompany.app.queue;


public class MyQueueImpl<T> implements MyQueue<T> {

    public int MAX_LENGTH = 5;
    public final Object[] queue;
    public int head = -1;
    public int tail = -1;

    public MyQueueImpl() {
        queue = new Object[MAX_LENGTH];
    }

    @Override
    public void enQueue(T val) {
        if (isFull()) {
            System.out.println("Queue is full, cannot insert anymore");
        } else {
            tail = ++tail % MAX_LENGTH;
            queue[tail] = val;
            if (head == - 1)
                head = tail;
        }
    }

    @Override
    public T deQueue() {
        T returnVal = null;
        if (isEmpty()) {
            System.out.println("Queue is empty, cannot get");
        } else {
            returnVal = (T)queue[head];
            if (head == tail) {
                head = -1;
                tail = -1;
            } else
                head = ++head % MAX_LENGTH;
        }
        return returnVal;
    }

    @Override
    public boolean isEmpty() {
        if (head == -1)
            return true;
        return false;
    }

    @Override
    public boolean isFull() {
        if (head == (tail + 1) % MAX_LENGTH)
            return true;
        return false;
    }

}

