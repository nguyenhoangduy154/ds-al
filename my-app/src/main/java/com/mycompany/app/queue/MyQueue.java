package com.mycompany.app.queue;

public interface MyQueue<T> {

    /**
     * Insert val to queue
     * @param val
     */
    void enQueue(T val);

    /**
     * Delete val from queue
     * @return
     */
    T deQueue();

    boolean isEmpty();

    boolean isFull();

}
