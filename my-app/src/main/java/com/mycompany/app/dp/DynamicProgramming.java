package com.mycompany.app.dp;

import java.util.ArrayList;
import java.util.List;

public class DynamicProgramming {

    // Top down approaches
    public static long fibExtTopDown(int n) {
        long[] ints = new long[n + 1];
        return fib(n, ints);
    }

    // Bottom up approaches
    public static long fibBottomUp(int n) {
        long[] longs = new long[n];
        longs[0] = 1;
        longs[1] = 1;
        for (int i = 2; i < n; i++) {
            longs[i] = longs[i - 1] + longs[i - 2];
        }
        return longs[n - 1];
    }

    private static long fib(int n, long[] lst) {
        if (lst[n] != 0)
            return lst[n];
        if (n == 1 || n == 2)
            return 1;
        long val = fib(n - 1, lst) + fib(n - 2, lst);
        lst[n] = val;
        return val;
    }
}
