package com.mycompany.app.sorting;

import java.util.List;

public interface Sorting {

     void sort(int[] arr);

}
