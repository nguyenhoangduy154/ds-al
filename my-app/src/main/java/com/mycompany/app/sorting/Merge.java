package com.mycompany.app.sorting;

public class Merge implements Sorting {


    @Override
    public void sort(int[] arr) {
        mergeSort(arr, 0, arr.length - 1);
    }


    private void merge(int[] arr, int lo, int mid, int hi) {
        System.out.println("[" + lo + ", " + hi + "]");
        int lLo = mid - lo + 1; // length of low arr
        int lHi = hi - mid; // length of hi arr

        int[] loArr = new int[lLo]; // temp loArr[1..mid]
        int[] hiArr = new int[lHi]; // temp hi arr[mid + 1...hi]

        // make a copy to temp arrs
        for (int i = 0; i < lLo; i++) {
            loArr[i] = arr[lo + i];
        }
        for (int i = 0; i < lHi; i++) {
            hiArr[i] = arr[i + mid + 1];
        }

        // merge
        int u = 0, v = 0;
        for (int i = lo; i <= hi ; i++) {
            if (u >= lLo) {
                arr[i] = hiArr[v++];
                continue;
            }

            if (v >= lHi) {
                arr[i] = loArr[u++];
                continue;
            }

            if (loArr[u] <= hiArr[v]) {
                arr[i] = loArr[u++];
            } else {
                arr[i] = hiArr[v++];
            }
        }
    }

    private void mergeSort(int[] arr, int lo, int hi) {
        if (lo < hi) {
            int mid = (hi + lo) / 2;
//            System.out.print(mid + ", ");
            mergeSort(arr, lo, mid);
            mergeSort(arr, mid + 1, hi);
            merge(arr, lo, mid, hi);
        }
    }
}
