package com.mycompany.app.queue;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QueueTest {

    MyQueue<Integer> q;


    @Before
    public void setUp() {
        q = new MyQueueImpl<>();
    }

    @After
    public void tearDown() {
        q = null;
    }

    @Test
    public void enqueueThenDequeue() {
        q.enQueue(1);
        q.enQueue(2);
        Assert.assertEquals(1, (int)q.deQueue());
        Assert.assertEquals(2, (int)q.deQueue());
    }

   @Test
   public void enqueueIfFull() {
       q.enQueue(9);
       q.enQueue(8);
       q.enQueue(7);
       q.enQueue(6);
       q.enQueue(5);
       q.enQueue(3);
       Integer[] expected = {9, 8, 7, 6, 5};
       Assert.assertArrayEquals(expected, ((MyQueueImpl)q).queue);

   }

   @Test
    public void dequeueIfEmpty() {
        Assert.assertNull(q.deQueue());
   }
}
