package com.mycompany.app.dp;

import com.mycompany.app.dp.DynamicProgramming;
import com.mycompany.app.dp.NoDynamicProgramming;
import org.junit.Assert;
import org.junit.Test;

public class DPTest {

    @Test
    public void executionTimeWithNoDP() {
//        long startTime = System.currentTimeMillis();
//        long result = NoDynamicProgramming.fib(45);
//        long endTime = System.currentTimeMillis();
//        System.out.println("No DP: " + (endTime - startTime) + " ms");
//        Assert.assertEquals(1134903170, result);
    }

    @Test
    public void executionTimeWithDP() {
        long startTime = System.currentTimeMillis();
        long result = DynamicProgramming.fibBottomUp(45);
        long endTime = System.currentTimeMillis();
        System.out.println("With DP: " + (endTime - startTime) + " ms");
        Assert.assertEquals(1134903170, result);
    }

}
