package com.mycompany.app.heap;

import static org.junit.Assert.assertTrue;

import com.mycompany.app.heap.MyPriorityQueue;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Unit test for simple App.
 */
@RunWith(JUnit4.class)
public class HeapTest
{

    MyPriorityQueue mpq;

    // the @before annotation is used for initiation of data before testing
    @Before
    public void init() {
        mpq = new MyPriorityQueue(20);
        mpq.offer(4);
        mpq.offer(1);
        mpq.offer(3);
        mpq.offer(2);
        mpq.offer(16);
        mpq.offer(9);
        mpq.offer(10);
        mpq.offer(14);
        mpq.offer(8);
        mpq.offer(7);
        mpq.offer(50);
        mpq.offer(1000);
    }

    @After
    public void tearDown() {
        mpq = null;
    }

    @Test
    public void mustReturnTheLargestValue()
    {
        Assert.assertEquals(1000, mpq.poll());
        Assert.assertEquals(50, mpq.poll());
    }

    @Test
    public void mustReturnTheSecondValue() {
    }
}
