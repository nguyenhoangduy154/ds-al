package com.mycompany.app.stack;

import com.mycompany.app.queue.MyQueueImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StackTest {

    MyStack<Integer> stack;

    @Before
    public void setUp() {
        stack = new MyStackImpl<>();
    }

    @After
    public void tearDown() {
        stack = null;
    }



    @Test
    public void pushThenPop() {
        stack.push(1);
        stack.push(2);
        Assert.assertEquals(2, (int)stack.pop());
        Assert.assertEquals(1, (int)stack.pop());
    }

    @Test
    public void pushIfFull() {
        stack.push(9);
        stack.push(8);
        stack.push(7);
        stack.push(6);
        stack.push(5);
        stack.push(4);
        Integer[] expected = {9, 8, 7, 6, 5};
        Assert.assertArrayEquals(expected, ((MyStackImpl)stack).s);

    }

    @Test
    public void popIfEmpty() {
        Assert.assertNull(stack.pop());
    }
}
