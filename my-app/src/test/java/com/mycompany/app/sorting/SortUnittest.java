package com.mycompany.app.sorting;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Random;

public class SortUnittest {

     Sorting merge = new Merge();
     int[] arr;

     @Before
     public void setUp() {
     }

     @After
     public void tearDown() {
     }

     @Test
     public void test1() {
         int[] arr = {5,8,4,2,1,6,5,3};
         int[] exp = {1,2,3,4,5,5,6,8};
         merge.sort(arr);
         Assert.assertArrayEquals(exp, arr);
     }
}
